# gg-app

Dockerizing the Flask application and deploying it to the Google cloud environment 


**What is Flask?**


[Flask ](https://flask.palletsprojects.com/en/2.0.x/)is a web framework, it’s a Python module that lets you develop web applications easily. It has a small and easy-to-extend core: a microframework that doesn’t include an ORM (Object Relational Manager) or such features.

****Installation Flask****

A virtual environment must be set up before downloading the flask. The more projects you have, the more likely it is to conflict with different versions of libraries and themselves. You can use a virtual environment to maintain compatibility in the project. Virtual environments are independent groups of Python libraries, one for each project. Packages installed for one project will not affect other projects or the operating system's packages. Python comes bundled with the venv module to create virtual environments.

**Creating an Environment**

For macOS/Linux
```
$ mkdir <directory-name>
$ cd <directory-name>
$ python3 -m venv venv
```

For Windows
```
> mkdir <directory-name>
> cd <directory-name>
> py -3 -m venv venv
```

Activate the environment

For macOS/Linux
`$ . venv/bin/activate`

For Windows
`venv\Scripts\activate`
Install Flask Withing the activated environment, use the following command to install Flask:
`$ pip install Flask`

Example:
Now let's look at a simple web application example. When this application runs, let's see this message on the screen "Hello GG from Gizem!" and let our application work on port 8585.
hello.py
  ```
from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello GG from Gizem!'

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8585, debug=True )

```
Next we create an instance of this class. The first argument is the name of the application’s module or package. name is a convenient shortcut for this that is appropriate for most cases. This is needed so that Flask knows where to look for resources such as templates and static files.

  `@app.route('/') We then use the route() decorator to tell Flask what URL should trigger our function.`

The function returns the message we want to display in the user’s browser. The default content type is HTML, so HTML in the string will be rendered by the browser.

app.run(host='0.0.0.0', port=8585, debug=True) we set our port
To run the application, use the flask command or python -m flask. Before you can do that you need to tell your terminal the application to work with by exporting the FLASK_APP environment variable:

For Bash:
```
$ export FLASK_APP=hello
$ flask run
```
For CMD
```
set FLASK_APP=hello
flask run
```
For Powershell
```
$env:FLASK_APP = "hello"
flask run
```
**Writing Dockerfile and Build Image**

[Docker](https://www.docker.com/) is an open platform for developing, shipping, and running applications. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly. With Docker, you can manage your infrastructure in the same ways you manage your applications. You can download and install Docker on multiple platforms.
Click the link for installation to Docker

_**Dockerize Flask Application**
**What is Dockerfile?**_
****
The Dockerfile is essentially the build instructions to build the image. We provide our images, which we will customize in Docker and use for our own purposes, with a text file called Dockerfile. In the Dockerfile it can contain all the commands that can be used to create an image. We use a structure called Dockerfile to ensure that these are created in one go and automatically.
saved Dockerfile

```
FROM python:3.6-alpine

LABEL maintainer="Gizem Özgün"
LABEL maintainer.mail="gizemozguun@gmail.com"

WORKDIR /app

COPY requirements.txt .
COPY hello.py .

RUN pip install -r requirements.txt
ENV FLASK_APP=hello.py
ENV HOST 0.0.0.0

EXPOSE 8585

CMD  flask run --port 8585

```
**Build the image**

We can create the image version of the file named Dockerfile that we wrote with the "docker build" command in Docker. The exact parameter the "docker build" command should take is where the Dockerfile is. For this, there can be a "path" after the "docker builds" command, that is, a path that tells where the file is in our local system, or a "URL" containing a git repository.

`$ docker build image -t yourusername/repository-name .`
you can check the images this command:

`$ docker images`

Now, we need a requirements.txt file again in the same directory. It will be enough to enter our flask version in the file where we can list these dependencies.
saved requirements.txt

`Flask==1.1.1`

Now our app works!

**_Using CI automate the container with GitLab_**

Push the files named Dockerfile, requirements.txt, and hello.py that you wrote on your computer to the repository you created.
Then click on the CI/CD section you will see in the left panel and click on editor. Now you should write gitlab-ci.yml file.
In the .gitlab-ci.yml file, you can define:
  The scripts you want to run.
  Other configuration files and templates you want to include.
  Dependencies and caches.
The commands you want to run in sequence and those you want to run in parallel.
The location to deploy your application to.
Whether you want to run the scripts automatically or trigger any of them manually. The scripts are grouped into jobs, and jobs run as part of a larger pipeline. You can group multiple independent jobs into stages that run in a defined order.
This is how I wrote the gitlab-ci.yaml file needed for this project:

```
stages:      # List of stages for jobs, and their order of execution
  - build
  - deploy
  
build:
  stage: build
  before_script: []
  image:
    name: docker:stable
  services:
   - name: docker:dind
     alias: thedockerhost
  variables:
    DOCKER_HOST: tcp://thedockerhost:2375/
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
  script:
    - ls -la
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
    - docker build -t "$CI_REGISTRY_IMAGE:latest" .
    - docker image ls
    - docker push $CI_REGISTRY_IMAGE:latest
deploy: 
  stage: deploy
  image: google/cloud-sdk
  before_script:
    - curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
  script:
    - gcloud auth activate-service-account --key-file ./gkecluster-keyfile.json
    - gcloud config set project gg-app-339518
    - gcloud config set compute/zone  europe-central2-a
    - gcloud container clusters get-credentials cluster-2
    - helm upgrade frontend-app-chart frontend-app-chart/ --values frontend-app-chart/values.yaml 
```
Here I defined the build and deploy steps.
The build result received as a login to Docker is pushed to the image Gitlab container registry. From here it is deployed to Google Cloud.

create values.yaml file

```
 This is a YAML-formatted file.
# Declare variables to be passed into your templates.

replicaCount: 1

image:
  repository: registry.gitlab.com/gizemozgun/gg-app
  pullPolicy: Always
  # Overrides the image tag whose default is the chart appVersion.
  tag: latest

imagePullSecrets: []
nameOverride: ""
fullnameOverride: ""

serviceAccount:
  # Specifies whether a service account should be created
  create: true
  # Annotations to add to the service account
  annotations: {}
  # The name of the service account to use.
  # If not set and create is true, a name is generated using the fullname template
  name: gg-service-account

podAnnotations: {}

podSecurityContext: {}
  # fsGroup: 2000

securityContext: {}
  # capabilities:
  #   drop:
  #   - ALL
  # readOnlyRootFilesystem: true
  # runAsNonRoot: true
  # runAsUser: 1000

service:
  type: LoadBalancer
  port: 80
  targetPort: 8585

ingress:
  enabled: false
  className: ""
  annotations: {}
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
  tls: []
  #  - secretName: chart-example-tls
  #    hosts:
  #      - chart-example.local

resources:
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
    #limits:
      #cpu: 100m
      #memory: 128Mi
    #requests:
      #cpu: 100m
      #memory: 128Mi

autoscaling:
  enabled: false
  minReplicas: 1
  maxReplicas: 3
  targetCPUUtilizationPercentage: 80
  # targetMemoryUtilizationPercentage: 80

nodeSelector: {}

tolerations: []

affinity: {}
```
create Chart.yaml

```
apiVersion: v2
name: frontend-app-chart
description: A Helm chart for Kubernetes

# A chart can be either an 'application' or a 'library' chart.
#
# Application charts are a collection of templates that can be packaged into versioned archives
# to be deployed.
#
# Library charts provide useful utilities or functions for the chart developer. They're included as
# a dependency of application charts to inject those utilities and functions into the rendering
# pipeline. Library charts do not define any templates and therefore cannot be deployed.
type: application

# This is the chart version. This version number should be incremented each time you make changes
# to the chart and its templates, including the app version.
# Versions are expected to follow Semantic Versioning (https://semver.org/)
version: 0.1.0

# This is the version number of the application being deployed. This version number should be
# incremented each time you make changes to the application. Versions are not expected to
# follow Semantic Versioning. They should reflect the version the application is using.
# It is recommended to use it with quotes.
appVersion: "1.16.0"
```

create deployment.yaml file

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "frontend-app-chart.fullname" . }}
  labels:
    {{- include "frontend-app-chart.labels" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "frontend-app-chart.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "frontend-app-chart.selectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "frontend-app-chart.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            - name: test
              containerPort: 8585
              protocol: TCP
          livenessProbe:
            tcpSocket:
              port: 8585
            initialDelaySeconds: 300
            periodSeconds: 30
          readinessProbe:
            tcpSocket:
              port: 8585
            initialDelaySeconds: 300
            periodSeconds: 30
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
```

create service.yaml file

```
apiVersion: v1
kind: Service
metadata:
  name: {{ include "frontend-app-chart.fullname" . }}
  labels:
    {{- include "frontend-app-chart.labels" . | nindent 4 }}
spec:
  type: {{ .Values.service.type }}
  ports:
    - port: {{ .Values.service.port }}
      targetPort: http
      protocol: TCP
      name: http
  selector:
    {{- include "frontend-app-chart.selectorLabels" . | nindent 4 }}
```
create ingress.yaml file

```
{{- if .Values.ingress.enabled -}}
{{- $fullName := include "frontend-app-chart.fullname" . -}}
{{- $svcPort := .Values.service.port -}}
{{- if and .Values.ingress.className (not (semverCompare ">=1.18-0" .Capabilities.KubeVersion.GitVersion)) }}
  {{- if not (hasKey .Values.ingress.annotations "kubernetes.io/ingress.class") }}
  {{- $_ := set .Values.ingress.annotations "kubernetes.io/ingress.class" .Values.ingress.className}}
  {{- end }}
{{- end }}
{{- if semverCompare ">=1.19-0" .Capabilities.KubeVersion.GitVersion -}}
apiVersion: networking.k8s.io/v1
{{- else if semverCompare ">=1.14-0" .Capabilities.KubeVersion.GitVersion -}}
apiVersion: networking.k8s.io/v1beta1
{{- else -}}
apiVersion: extensions/v1beta1
{{- end }}
kind: Ingress
metadata:
  name: {{ $fullName }}
  labels:
    {{- include "frontend-app-chart.labels" . | nindent 4 }}
  {{- with .Values.ingress.annotations }}
  annotations:
    {{- toYaml . | nindent 4 }}
  {{- end }}
spec:
  {{- if and .Values.ingress.className (semverCompare ">=1.18-0" .Capabilities.KubeVersion.GitVersion) }}
  ingressClassName: {{ .Values.ingress.className }}
  {{- end }}
  {{- if .Values.ingress.tls }}
  tls:
    {{- range .Values.ingress.tls }}
    - hosts:
        {{- range .hosts }}
        - {{ . | quote }}
        {{- end }}
      secretName: {{ .secretName }}
    {{- end }}
  {{- end }}
  rules:
    {{- range .Values.ingress.hosts }}
    - host: {{ .host | quote }}
      http:
        paths:
          {{- range .paths }}
          - path: {{ .path }}
            {{- if and .pathType (semverCompare ">=1.18-0" $.Capabilities.KubeVersion.GitVersion) }}
            pathType: {{ .pathType }}
            {{- end }}
            backend:
              {{- if semverCompare ">=1.19-0" $.Capabilities.KubeVersion.GitVersion }}
              service:
                name: {{ $fullName }}
                port:
                  number: {{ $svcPort }}
              {{- else }}
              serviceName: {{ $fullName }}
              servicePort: {{ $svcPort }}
              {{- end }}
          {{- end }}
    {{- end }}
{{- end }}
```

create hpa.yaml file

```
{{- if .Values.autoscaling.enabled }}
apiVersion: autoscaling/v2beta1
kind: HorizontalPodAutoscaler
metadata:
  name: {{ include "frontend-app-chart.fullname" . }}
  labels:
    {{- include "frontend-app-chart.labels" . | nindent 4 }}
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: {{ include "frontend-app-chart.fullname" . }}
  minReplicas: {{ .Values.autoscaling.minReplicas }}
  maxReplicas: {{ .Values.autoscaling.maxReplicas }}
  metrics:
    {{- if .Values.autoscaling.targetCPUUtilizationPercentage }}
    - type: Resource
      resource:
        name: cpu
        targetAverageUtilization: {{ .Values.autoscaling.targetCPUUtilizationPercentage }}
    {{- end }}
    {{- if .Values.autoscaling.targetMemoryUtilizationPercentage }}
    - type: Resource
      resource:
        name: memory
        targetAverageUtilization: {{ .Values.autoscaling.targetMemoryUtilizationPercentage }}
    {{- end }}
{{- end }}
```

create serviceaccount.yaml file

```
{{- if .Values.serviceAccount.create -}}
apiVersion: v1
kind: ServiceAccount
metadata:
  name: {{ include "frontend-app-chart.serviceAccountName" . }}
  labels:
    {{- include "frontend-app-chart.labels" . | nindent 4 }}
  {{- with .Values.serviceAccount.annotations }}
  annotations:
    {{- toYaml . | nindent 4 }}
  {{- end }}
{{- end }}
```


I used Helm and Kubernetes for the administration of the application. You can click for information and installations about [Kubernetes](https://kubernetes.io/) and [Helm](https://helm.sh/) and I did these works on [Google Cloud Platform](https://cloud.google.com/) Kubernetes Engine. 

In GCP, we first create a cluster:
- Select Kubernetes Engine from the left menu.
- After switching Kubernetes engine, click Cluster in the new left menu.
- Click Create Cluster.
It may take some time for your cluster to form. 
We use the cluster name and location zone information here for authentication in the deploy step in our GitLab-ci.yaml file. 

After deploying in GitLab, come to GCP. You can see and manage cluster, pod, container status by clicking Workload from the left menu.
