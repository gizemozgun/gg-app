FROM python:3.6-alpine

LABEL maintainer="Gizem Özgün"
LABEL maintainer.mail="gizemozguun@gmail.com"

WORKDIR /app

COPY requirements.txt .
COPY hello.py .

RUN pip install -r requirements.txt
ENV FLASK_APP=hello.py
ENV HOST 0.0.0.0

EXPOSE 8585

CMD  flask run --port 8585
